﻿using System;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace Glitchy
{
	static class Serializer
	{
		public static Byte[] Serialize(Bitmap image)
		{
			using (MemoryStream ms = new MemoryStream())
			{
				image.Save(ms, ImageFormat.Png);
				image = new Bitmap(Image.FromStream(ms));
			}

			var converter = new ImageConverter();
			return (byte[])converter.ConvertTo(image, typeof(byte[]));
		}

		public static Image Deserialize (byte[] b)
		{
			using (var ms = new MemoryStream(b))
			{
				return new Bitmap(Image.FromStream(ms));
			}
		}
	}
}
