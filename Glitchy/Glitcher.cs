﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace Glitchy
{
	static class Glitcher
	{
		public static Image Convert(Bitmap image)
		{
			var byteArray = Serializer.Serialize(image);
			byteArray = Glitch(byteArray, 0, 500);
			return Serializer.Deserialize(byteArray);
		}

		private static byte[] Glitch(byte[] byteArray, int headerSize, int offset)
		{
			var glitchedArrayHeader = byteArray.Take(headerSize).ToArray();
			var glitchedArrayBody = byteArray.Skip(headerSize).ToArray();

			ApplyNoise(ref glitchedArrayBody, 0.99995, 20);
			DestroyRegions(ref glitchedArrayBody, 0.9999995, Byte.Parse("128"));

			glitchedArrayBody = glitchedArrayBody.LetTheThreadDoTheTalk(0.9999);

			return glitchedArrayHeader.Concat(glitchedArrayBody).ToArray();
		}

		private static void ApplyNoise(ref byte[] byteArray, double prop, int offset)
		{
			var random = new Random();

			for (var i = 0; i < byteArray.Length - 1; i++)
			{
				if (random.NextDouble() > prop)
				{
					byteArray[i] = byteArray[i + offset > byteArray.Length - 1 ? i - offset : i + offset];
				}
			}
		}

		private static void DestroyRegions(ref byte[] byteArray, double prop, byte Color)
		{
			var random = new Random();

			for (var i = 0; i < byteArray.Length - 1; i++)
			{
				if (random.NextDouble() > prop)
				{
					var jump = random.Next(20, 50);
					var value = new byte[1];
					random.NextBytes(value);
					for (var j = i; (j < i + jump) && (i + jump < byteArray.Length - 1); j++)
					{
						byteArray[j] = Color;
					}
					i += jump;
				}
			}
		}

		private static byte[] LetTheThreadDoTheTalk(this byte[] byteArray, double prop)
		{
			var random = new Random();
			var newByteArray = new byte[byteArray.Length];
			var count = 0;
			Task.Factory.StartNew(() =>
			{
				for (var i=0; i < byteArray.Length -1; i += 2)
				{
					newByteArray[count] = byteArray[i];
				}
			});

			Task.Factory.StartNew(() =>
			{
				for (var i = 1; i < byteArray.Length - 1; i += 2)
				{
					newByteArray[count] = byteArray[i];
				}
			});

			for (var i = 0; i < byteArray.Length - 1; i+= 8)
			{
				if (random.NextDouble() > prop)
				{
					byteArray[i] = newByteArray[i];
					byteArray[i+1] = newByteArray[i+1];
					byteArray[i+2] = newByteArray[i+2];
					byteArray[i+3] = newByteArray[i+3];
					byteArray[i+4] = newByteArray[i+4];
					byteArray[i+5] = newByteArray[i+4];
					byteArray[i+6] = newByteArray[i+6];
					byteArray[i+7] = newByteArray[i+7];
				}
			}

			return byteArray;
		}
	}
}
