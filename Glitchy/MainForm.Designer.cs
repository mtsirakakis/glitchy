﻿namespace Glitchy
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.openFileButton = new System.Windows.Forms.Button();
			this.convertButton = new System.Windows.Forms.Button();
			this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.pictureBox = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
			this.SuspendLayout();
			// 
			// openFileDialog
			// 
			this.openFileDialog.FileName = "openFileDialog";
			// 
			// openFileButton
			// 
			this.openFileButton.BackColor = System.Drawing.Color.Transparent;
			this.openFileButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.openFileButton.Location = new System.Drawing.Point(12, 472);
			this.openFileButton.Name = "openFileButton";
			this.openFileButton.Size = new System.Drawing.Size(84, 23);
			this.openFileButton.TabIndex = 2;
			this.openFileButton.Text = "Open File";
			this.openFileButton.UseVisualStyleBackColor = false;
			this.openFileButton.Click += new System.EventHandler(this.openFileButton_Click);
			// 
			// convertButton
			// 
			this.convertButton.BackColor = System.Drawing.Color.Transparent;
			this.convertButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.convertButton.ForeColor = System.Drawing.SystemColors.ControlText;
			this.convertButton.Location = new System.Drawing.Point(102, 472);
			this.convertButton.Name = "convertButton";
			this.convertButton.Size = new System.Drawing.Size(75, 23);
			this.convertButton.TabIndex = 3;
			this.convertButton.Text = "Glitch";
			this.convertButton.UseVisualStyleBackColor = false;
			this.convertButton.Click += new System.EventHandler(this.convertButton_Click);
			// 
			// saveFileDialog
			// 
			this.saveFileDialog.Filter = "Bitmap FIles|*bmp";
			// 
			// pictureBox
			// 
			this.pictureBox.BackColor = System.Drawing.Color.Aquamarine;
			this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pictureBox.Location = new System.Drawing.Point(12, 12);
			this.pictureBox.Name = "pictureBox";
			this.pictureBox.Size = new System.Drawing.Size(505, 454);
			this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox.TabIndex = 4;
			this.pictureBox.TabStop = false;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.ClientSize = new System.Drawing.Size(556, 507);
			this.Controls.Add(this.pictureBox);
			this.Controls.Add(this.convertButton);
			this.Controls.Add(this.openFileButton);
			this.Name = "MainForm";
			this.Text = "Glitchy";
			((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.OpenFileDialog openFileDialog;
		private System.Windows.Forms.Button openFileButton;
		private System.Windows.Forms.Button convertButton;
		private System.Windows.Forms.SaveFileDialog saveFileDialog;
		private System.Windows.Forms.PictureBox pictureBox;
	}
}

