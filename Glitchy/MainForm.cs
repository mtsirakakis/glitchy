﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace Glitchy
{
	public partial class MainForm : Form
	{
		private static Bitmap ImageToConvert;

		public MainForm()
		{
			InitializeComponent();
		}

		private void openFileButton_Click(object sender, EventArgs e)
		{
			try
			{
				openFileDialog.ShowDialog();
				ImageToConvert = (Bitmap)Image.FromFile(openFileDialog.FileName);
				pictureBox.Image = ImageToConvert;
			}
			catch
			{
				MessageBox.Show("Invalid File!", "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void convertButton_Click(object sender, EventArgs e)
		{
			try
			{
				pictureBox.Image = Glitcher.Convert(ImageToConvert);
			}
			catch
			{
				MessageBox.Show("Failed To Glitch this Image!", "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void SaveToImage(Image image)
		{
			if(saveFileDialog.ShowDialog() == DialogResult.OK) image.Save(saveFileDialog.FileName+".bmp", ImageFormat.Bmp);
		}
	}
}
